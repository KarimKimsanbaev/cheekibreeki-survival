﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(PortServerUtility))]
public class PortServerUtilityCustomize : Editor
{
	public override void OnInspectorGUI()
	{ //Сообщаем редактору, что этот инспектор заменит прежний (встроеный)
		DrawDefaultInspector();//отрисовка содержимого инспектора по умолчанию

		if (GUILayout.Button("Наша кнопка"))
		{
			Debug.Log(1);
		}
	}
}