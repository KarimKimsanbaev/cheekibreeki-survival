﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HistoryWidgetView : ListView<HistoryItem, HistoryWidgetItemView>
{
    [SerializeField] GameCoreController GameController;
    private IGameCoreObserverable _gameCoreObserverable;
    
    private void Start()
    {
        _gameCoreObserverable = GameController.GetObserverableProvider();
        _gameCoreObserverable.AskNumberEvent += _OnAskNumber;

        _gameCoreObserverable.ReceivePlayerAnswerEvent += _OnPlayerAnswer;
        _gameCoreObserverable.OnGameOver += _OnGameOver; ;
    }

    private void _OnGameOver(GameOverType type)
    {
        _itemView.gameObject.SetActive(true);
    }

    private HistoryItem _item;
    private HistoryWidgetItemView _itemView;
    private void _OnAskNumber(int num, string rightAnswer)
    {
        _item = new HistoryItem(num, rightAnswer);
        _itemView = GetNewItem("–", _item, _sourceView);
        _itemView.gameObject.SetActive(false);
    }

    private void _OnPlayerAnswer(string playerAnswer)
    {
        _item.SetPlayerAnswer(playerAnswer);
        ((IItemView<HistoryItem>)_itemView).SetContent(_item);
        _itemView.gameObject.SetActive(true);
    }

    HistoryWidgetItemView GetNewItem(string playerAnswer, HistoryItem item, HistoryWidgetItemView sourceView)
    {
        HistoryWidgetItemView itemView = null;
        if (item != null)
        {
            itemView = ((IListView<HistoryItem, HistoryWidgetItemView>)this).CreateItem(item, sourceView);
        }
        return itemView;
    }

    protected override void SetSourceData(IItemView<HistoryItem> view, HistoryItem item)
    {
        ((IItemView<HistoryItem>)view).SetContent(item);
    }
}
