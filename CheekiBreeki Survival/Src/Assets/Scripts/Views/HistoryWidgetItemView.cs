﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HistoryWidgetItemView : MonoBehaviour, IItemView<HistoryItem>
{
    [SerializeField] Text Num;
    [SerializeField] Text RightAnswer;
    [SerializeField] Text PlayerAnswer;

    GameObject IItemView<HistoryItem>.GetGameObject()
    {
        return gameObject;
    }

    void IItemView<HistoryItem>.SetContent(HistoryItem item)
    {
        Num.SetText(item.Num.ToString());
        RightAnswer.SetText(item.RightAnswer);
        PlayerAnswer.SetText(item.PlayerAnswer);
    }
}
