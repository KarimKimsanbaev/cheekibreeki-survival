﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System;

public class CheekiBreekiView : MonoBehaviour
{
    [SerializeField] private GameCoreController GameController;

    [SerializeField] private Text NumberText;
    [SerializeField] private Text ResultText;
    [SerializeField] private Text AnswerInfoText;

    [SerializeField] GameObject _buttonCanvasGO;

    private IGameCoreObserverable GameCore;

    public int Number;
    public string Result;

    

    void Awake()
    {
        GameCore = GameController.GetCoreObserverable();

        GameCore = GameController.GetObserverableProvider();

        GameCore.AskNumberEvent += _OnAskNumber;

        GameCore.OnRightAnswer += _OnRightAnswer;
        GameCore.OnWrongAnswer += _OnWrongAnswer;

        GameCore.OnGameOver += _OnGameOver;

    }

    private void _OnGameOver(GameOverType type)
    {
        ShowResultWrongAnswer(GameCore.Result);
    }

    private void _OnRightAnswer()
    {
        ShowResultRightAnswer(GameCore.Result);
    }
    private void _OnWrongAnswer()
    {
        ShowResultWrongAnswer(GameCore.Result);
    }

    private void _OnAskNumber(int num, string result)
    {
        Number = num;

        NumberText.SetText(num.ToString());
        ResultText.SetText(result, false);

        AnswerInfoText.gameObject.SetActive(false);
        _buttonCanvasGO.SetActive(true);
    }

    public void ShowResultRightAnswer(string result)
    {
        if (ResultText == null)
        {
            Debug.LogError("Result field Null Reference!");
            return;
        }

        ResultText.text = string.Format("{0} - Ответ правильный!", result);
        ResultText.gameObject.SetActive(true);

        AnswerInfoText.text = "Ответ правильный!";
        AnswerInfoText.gameObject.SetActive(true);
    }

    public void ShowResultWrongAnswer(string result)
    {
        if (ResultText == null)
        {
            Debug.LogError("Result field Null Reference!");
            return;
        }

        ResultText.text = string.Format("{0}", result);
        ResultText.gameObject.SetActive(true);

        AnswerInfoText.text = "Не правильный ответ! Вы проиграли.";
        AnswerInfoText.gameObject.SetActive(true);
    }
}
