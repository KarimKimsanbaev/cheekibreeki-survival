﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpecialButtonView : MonoBehaviour
{
    [SerializeField] GameCoreController controller;

    [SerializeField] public ButtonAnswer TypeButton;
    [SerializeField] public Text TextButton;

    [SerializeField] public CheekiBreekiView CheekiBreekiView;

    private void FixedUpdate()
    {
        if (TypeButton == ButtonAnswer.Number)
            ChangeTextButton(CheekiBreekiView.Number);
    }

    public void OnClick()
    {
        if (TypeButton == ButtonAnswer.Number)
        {
            ChangeTextButton(CheekiBreekiView.Number);
            controller.OnClickButtonWithAnswer(CheekiBreekiView.Number.ToString());
        }
    }

    void ChangeTextButton(int num)
    {
        TextButton.text = num.ToString();
    }
}
