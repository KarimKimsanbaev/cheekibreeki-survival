﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class LeaderboardItemView : MonoBehaviour, IItemView<PlayerSessionReport>
{
    [SerializeField] public Text PlayerName;
    [SerializeField] public Text TotalSessionTime;
    [SerializeField] public Text TotalRightAnswer;

    GameObject IItemView<PlayerSessionReport>.GetGameObject()
    {
        if (gameObject != null)
            return gameObject;
        else
        {
            Debug.LogError(string.Format("Game Object in with {0} is null, but trying to access it.", this.ToString()));
            return null;
        }
    }

    void IItemView<PlayerSessionReport>.SetContent(PlayerSessionReport item)
    {
        PlayerName.text = item.id.ToString();
        TotalSessionTime.text = item.totalSessionTime.ToString();
        TotalRightAnswer.text = item.totalRightAnswer.ToString();
    }
}
