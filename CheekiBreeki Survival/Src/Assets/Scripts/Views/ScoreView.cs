﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreView : MonoBehaviour
{
    [SerializeField] Text _scoreText;
    [SerializeField] Text _timeText;
    [SerializeField] Text _statusText;

    [SerializeField] int _totalRightAnswer;
    [SerializeField] int _time;

    [SerializeField] CheekiBreekiView _view;

    [SerializeField] GameCoreController GameController;

    private IGameCoreObserverable _gameCore;

    void Start()
    {
        _InitEventHandlers();
    }

    private void _InitEventHandlers()
    {
        _gameCore = GameController.GetObserverableProvider();

        _gameCore.OnRightAnswer += _OnRigthAnswerAction;
        _gameCore.OnGameOver += _OnGameOver;
    }

    private void _OnRigthAnswerAction()
    {
        _totalRightAnswer++;
        UpdateScoreText();
    }

    private void _OnGameOver(GameOverType obj)
    {
        _statusText.text = "Вы проиграли";
        _gameOver = true;

        StartCoroutine( new WebService().SendSessionReport(new PlayerSessionReport()
        {
            totalRightAnswer = _totalRightAnswer,
            totalSessionTime = _time
        }));
    }




    bool _gameOver = false;
    void FixedUpdate()
    {
        if (_gameOver == false)
        {
            UpdateTimeText();
        }
    }

    void UpdateTimeText()
    {
        _time = System.Convert.ToInt32(UnityEngine.Time.time);
        _timeText.text = $"Время игры: {_time.ToString()}";
    }
    void UpdateScoreText()
    {
        _scoreText.text = $"Ответы: {_totalRightAnswer.ToString()}";
    }
}
