﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class LeaderboardView : ListView<PlayerSessionReport, LeaderboardItemView>
{
    public void Button_OnClick()
    {
        var RS = new WebService();

        RS.GetLeaderboardCompleted += FillLeaderboard;

        var t = StartCoroutine(RS.GetLeaderboard());
    }

    public void ButtonClose_OnClick()
    {
        CleanLeaderboard();
    }

    private void FillLeaderboard(PlayerSessionReport[] reports)
    {
        for (int i = 0; i < reports.Length; i++)
        {
            ((IListView<PlayerSessionReport, LeaderboardItemView>)this).CreateItem(reports[i], _sourceView);
        }
    }

    private void CleanLeaderboard()
    {
        int countDestroyItems = ((IListView<PlayerSessionReport, LeaderboardItemView>)this).DestroyItems();
        Debug.Log(string.Format("Detroy {0} items", countDestroyItems));
    }

    protected override void SetSourceData(IItemView<PlayerSessionReport> view, PlayerSessionReport item)
    {
        ((IItemView<PlayerSessionReport>)view).SetContent(item);
    }
}
