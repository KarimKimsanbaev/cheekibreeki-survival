﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGameCoreController
{
    string GetRightAnswer(int num);
    void AskNumber(int number);
    void RightAnswer();
    void GameOver(GameOverType type);
    void SetPause(bool pause);
}
