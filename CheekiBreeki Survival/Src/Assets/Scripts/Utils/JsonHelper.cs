﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class JsonHelper
{
    public static T[] FromJsonArray<T>(string json)
    {
        var jsonFixed = fixJson(json);
        Wrapper<T> wrapper = UnityEngine.JsonUtility.FromJson<Wrapper<T>>(jsonFixed);
        return wrapper.Items;
    }

    static string fixJson(string value)
    {
        value = "{\"Items\":" + value + "}";
        return value;
    }

    public static string ToJson<T>(T[] array)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.Items = array;
        return JsonUtility.ToJson(wrapper);
    }

    public static string ToJson<T>(T[] array, bool prettyPrint)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.Items = array;
        return JsonUtility.ToJson(wrapper, prettyPrint);
    }

    [Serializable]
    private class Wrapper<T>
    {
        public T[] Items;
    }
}
