﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class WebService : IWebService
{
	public IEnumerator SendSessionReport(PlayerSessionReport report)
	{
		var jsonString = JsonUtility.ToJson(report);

		byte[] byteData = System.Text.Encoding.UTF8.GetBytes(jsonString);

		UnityWebRequest unityWebRequest = new UnityWebRequest(ConstantDataServer.SERVER_LEADERBOARD_URL, UnityWebRequest.kHttpVerbPOST);

		unityWebRequest.uploadHandler = new UploadHandlerRaw(byteData);
		unityWebRequest.downloadHandler = new DownloadHandlerBuffer();
		unityWebRequest.uploadHandler.contentType = "application/json";

		unityWebRequest.SetRequestHeader("Content-Type", "application/json");



		yield return unityWebRequest.SendWebRequest();

		if (unityWebRequest.isNetworkError || unityWebRequest.isHttpError)
		{
			Debug.Log(unityWebRequest.error);
		}
		else
		{
			Debug.Log("SessionReport upload complete! Status Code: " + unityWebRequest.responseCode);
		}
	}



	public static PlayerSessionReport[] reports;

	public event System.Action<PlayerSessionReport[]> GetLeaderboardCompleted;
	public IEnumerator GetLeaderboard()
	{
		UnityWebRequest www = UnityWebRequest.Get(ConstantDataServer.SERVER_LEADERBOARD_URL_GET_SORT);
		yield return www.SendWebRequest();

		if (www.isNetworkError || www.isHttpError)
		{
			Debug.Log(www.error);
			Debug.Log(www.downloadHandler.text);
		}
		else
		{

			string lb = www.downloadHandler.text;

			reports = JsonHelper.FromJsonArray<PlayerSessionReport>(lb);
			GetLeaderboardCompleted(reports);
		}
	}
}
